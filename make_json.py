import json

#  CREATE USER 'temp_admin'@'localhost' IDENTIFIED BY 'dPignw4u,c2c';
#  GRANT ALL PRIVILEGES ON temperaturverwaltung. * TO 'temp_admin'@'localhost';

class MakeJson:
    db_credentials = {"username": "temp_admin",
                      "password": "dPignw4u,c2c",
                      "db": "temperaturverwaltung",
                      "host": "localhost"}
    filename = 'db_credentials.json'

    def __init__(self):
        with open(self.filename, 'w') as credentials_file:
            json.dump(self.db_credentials, credentials_file)


if __name__ == '__main__':
    MakeJson()
