import mysql.connector as mysql_connector
from io_cli import IoCli as IO
import json


class TempDatabase:
    def __init__(self):
        try:
            with open('db_credentials.json') as credentials_file:
                credentials = json.load(credentials_file)
        except FileNotFoundError:
            print("No credentials for db found. Try make it with make_json.py")
            exit(0)
        print('connecting to database')
        # credentials = read_credentials()
        self.connection = mysql_connector.connect(
            user=credentials['username'],
            password=credentials['password'],
            database=credentials['db'],
            host=credentials['host']
        )

    def get_admins(self):
        db_cursor = self.connection.cursor()
        query = "SELECT ID, password, phone_number, login_name, name FROM admin"
        db_cursor.execute(query)
        admin_list = []
        for ID, password, phone_number, login_name, name in db_cursor:
            admin = {"ID": ID, "password": password, "phone_number": phone_number, "login_name": login_name, "name": name}
            admin_list.append(admin)
        return admin_list

    def get_admins_list(self):
        db_cursor = self.connection.cursor()
        query = "SELECT ID, login_name, name, password, phone_number FROM admin"
        db_cursor.execute(query)
        admin_list = []
        for ID, login_name, name, password, phone_number in db_cursor:
            admin = {"ID": ID, "login_name": login_name, "name": name, "password": password, "phone_number": phone_number}
            admin_list.append(admin)
        return admin_list

    def get_users(self):
        db_cursor = self.connection.cursor()
        query = "SELECT ID, login_name, name FROM user"
        db_cursor.execute(query)
        user_list = []
        for ID, login_name, name in db_cursor:
            user = {"ID": ID, "login_name": login_name, "name": name}
            user_list += user
            print(f'ID: {ID}, login name: {login_name}, name: {name}')
        return user_list

    def new_admin(self, newadmin):
        db_cursor = self.connection.cursor()
        query = "INSERT INTO admin (name, login_name, phone_number, password) " \
                "VALUES ( '" + \
                newadmin["real_name"] + "', '" + \
                newadmin["admin_name"] + "', '" + \
                newadmin["phone_number"] + "', '" + \
                newadmin["admin_pass"] + "')"
        db_cursor.execute(query)
        self.connection.commit()
        db_cursor.close()

    def get_sensors_list(self):
        db_cursor = self.connection.cursor()
        query = "SELECT ID, sensor_number, address, max_temperature from sensor"
        db_cursor.execute(query)
        sensor_list = []
        for ID, sensor_number, address, max_temperature in db_cursor:
            sensor_list.append({"ID": ID, "sensor_number": sensor_number, "address": address, "max_temperature": max_temperature})

        return sensor_list

    def update_temperature(self, sensor_id, max_temp):
        db_cursor = self.connection.cursor()
        max_temp = str(max_temp)
        sensor_id = str(sensor_id)
        query = "UPDATE sensor SET max_temperature = '" + max_temp + "' WHERE ID = " + sensor_id
        # print(query)
        db_cursor.execute(query)
        self.connection.commit()
        db_cursor.close()
