class Texts:
    note_welcome = "You are administrating the Temperature program now.\n" \
                   " Please remember:\n " \
                   "With great power comes great responsibility\n\n"
    note_help = "commands: \n" \
                "\"help\" - shows this\n" \
                "\"show max temps\" - show temperatures\n" \
                "\"change max temps\" - change temperatures\n" \
                "\"add admin\" - add administrator\n" \
                "\"show admins\" - show users/administrators\n" \
                "\"mod admin\" - modify users/administrators\n" \
                "\"delete admin\" - modify users/administrators\n" \
                "\"exit\" - quit program"
