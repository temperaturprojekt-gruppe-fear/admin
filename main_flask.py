from db import TempDatabase
from io_cli import IoCli as IO
from texts import Texts as T
from flask import Flask, redirect, url_for, render_template, request, session, flash
from datetime import timedelta

app = Flask(__name__)   # this is the VIEW
app.secret_key = "blahblah"
app.permanent_session_lifetime = timedelta(minutes=20)
db = TempDatabase()     # this is the MODEL


@app.route("/")
def landing_page():
    if "admin" in session:
        return redirect(url_for("admin_page"))
    else:
        return redirect(url_for("login_page"))


@app.route("/login", methods=["POST", "GET"])
def login_page():
    admin_list = db.get_admins()
    if request.method == "POST":
        session.permanent = True
        admin_name = request.form["admin_name"]
        admin_pass = request.form["admin_pass"]
        for admin_line in admin_list:
            if admin_name == admin_line["login_name"]:
                if admin_pass == admin_line["password"]:
                    session["admin_name"] = admin_name
                    flash("Login successful!")
                    return redirect(url_for("admin_page"))
                else:
                    flash("Wrong password!")
                    return redirect(url_for("login_page"))

        flash(f"Credentials for {admin_name} not found.", "info")
        return redirect(url_for("login_page"))
    else:
        if "admin_name" in session:
            admin_name = session["admin_name"]
            flash(f"Already logged in. If you are not {admin_name} please log out.")
            return redirect(url_for("admin_page"))

        return render_template("login.html")


@app.route("/logout")
def logout_page():
    if "admin_name" in session:
        admin_name = session["admin_name"]
        session.pop("admin_name")
        flash(f"{admin_name} was logged out successfully.")
        return redirect(url_for("login_page"))
    else:
        flash(f"There os no active user, who could be logged out.")
        return redirect(url_for("login_page"))


@app.route("/admin")
def admin_page():
    if "admin_name" in session:
        admin_name = session["admin_name"]
        return render_template("admin.html", admin_name=admin_name)
    else:
        return redirect(url_for("login_page"))


@app.route("/accounts", methods=["POST", "GET"])
def accounts_page():
    if "admin_name" in session:
        if request.method == "POST":
            print(request.form)
            print("has type: " + str(type(request.form)))

            new_admin = {"admin_name": request.form["admin_name"],
                         "real_name": request.form["real_name"],
                         "admin_pass": request.form["admin_pass"],
                         "phone_number": request.form["phone_number"]}
            db.new_admin(new_admin)

        admin_name = session["admin_name"]
        dataset = db.get_admins_list()
        return render_template("accounts.html", admin_name=admin_name, dataset=dataset)
    else:
        return redirect(url_for("login_page"))


@app.route("/sensors", methods=["POST", "GET"])
def sensors_page():
    if "admin_name" in session:
        admin_name = session["admin_name"]
        if request.method == "POST":
            sensor_form = request.form
            # print(answer)
            # print(answer.to_dict(flat=False))
            sensor_dict = sensor_form.to_dict(flat=False)
            for sensor_key in sensor_dict.keys():
                sensor_id = sensor_key
                max_temp = sensor_dict[sensor_id]
                max_temp = max_temp[0]

            print("sensor id: " + str(sensor_id) + " has type " + str(type(sensor_id)))
            print("max temp: " + str(max_temp) + " has type " + str(type(max_temp)))
            db.update_temperature(sensor_id, max_temp)
            log_file = open("max_temp_changes.log", "a")
            log_file.write(admin_name + " has changed max temperature of Sensor "
                           + str(sensor_id) + " to " + str(max_temp) + ".\n")
            log_file.close()
        dataset = db.get_sensors_list()
        return render_template("sensors.html", admin_name=admin_name, dataset=dataset)
    else:
        return redirect(url_for("login_page"))


if __name__ == '__main__':
    app.run(debug=True)

