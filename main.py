
from db import TempDatabase
from io_cli import IoCli as IO
from texts import Texts as T
from flask import Flask, redirect, url_for, render_template

app = Flask(__name__)
db = TempDatabase()

def read_db_credentials():
    try:
        with open('db_credentials.json') as credentials_file:
            credentials = json.load(credentials_file)
            return credentials
    except FileNotFoundError:
        print("No credentials for db found. Try make it with make_json.py")

@app.route("/")
def landing_page():
    return render_template("index.html")


@app.route("/login")
def login_page(db):
    admin_list = db.get_admins()
    print(admin_list)
    print("type of user_list: " + str(type(admin_list)))
    print('typing  \"quit\" as username makes the program stop')
    while True:
        username = input("username: \n")
        if username in ["quit", "exit", "q"]:
            exit(0)
        password = input('password: \n')
        for admin in admin_list:
            if username == admin["login_name"] and password == admin["password"]:
                main_loop(db)
        print("username or password wrong\nplease try again or type \"quit\" to quit.")


def add_admin(db):
    while True:
        newadmin = {"login_name": "", "real_name": "", "phone_number": "", "password": "password"}
        newadmin["login_name"] = IO.input_mod("login name: ")
        login_name_list = []
        for ID, password, phone_number, login_name, name in db.get_admins():
            login_name_list.append(login_name)
        if newadmin["login_name"] in  login_name_list:
            IO.print_mod("login name already exists")
            continue

    newadmin["real name"] = IO.input_mod("real name: ")
    newadmin["phone number"] = IO.input_mod("phone number: ")
    newadmin["password"] = IO.input_mod("password: ")
    db_answer = db.new_admin(newadmin)
    IO.print_mod(db_answer)


def change_max_temp(db):
    IO.print_mod(db.get_max_temps)


def main_loop(db):
    print("i'm the main loop")
    print(T.note_welcome + T.note_help)
    while True:
        typing = IO.input_mod("please choose an option\n")
        if typing in ["q", "quit", "exit"]:
            exit(0)
        if typing == "help":
            print(T.note_help)
            continue
        if typing == "show max temps":
            db.get_max_temps()
            continue
        if typing == "show admins":
            admin_list = db.get_admins_list()
            IO.print_mod(admin_list)
            continue
        if typing == "add admin":
            add_admin(db)
            continue
        if typing == "change max temp":
            change_max_temp(db)
            continue
        IO.print_mod("You typed \"" + str(typing) + "\", which isn't a valid command. (Or not implemented yet)")


# def run():
    # db_credentials = read_db_credentials()  # for database connection
    # db = TempDatabase(db_credentials)
    # login(db)


if __name__ == '__main__':
    app.run()

